package co.com.sofka.pageobjets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ValidarPreciosPage extends PageObject{
	@FindBy(xpath="//*[@class='sf-with-ul' and text()='Women']")
	WebElementFacade btnTipo;
	
	@FindBy(xpath="//*[@class='sf-with-ul' and text()='Tops']/../ul/li[1]/a")
	WebElementFacade btnSubMenuTshirts;
	
	@FindBy(xpath="//*[@class='sf-with-ul' and text()='Tops']/../ul/li[2]/a")
	WebElementFacade btnSubMenuBlouses;

	@FindBy(className="product-container")
	WebElementFacade btnPrimerElemento;
	
	@FindBy(xpath="//*[@class='button-container']/a/span[text()='Add to cart']")
	WebElementFacade btnAddToCart;

	@FindBy(className="ajax_block_products_total")
	WebElementFacade lblSubtotal;
	
	@FindBy(xpath="//*[@id='layer_cart']/div[1]/div[2]/div[2]/span")
	WebElementFacade lblIva;

	@FindBy(xpath="//*[@id='layer_cart']/div[1]/div[2]/div[3]/span")
	WebElementFacade lblTotal;
	
	public void btnTipo() {
		withAction().moveToElement(btnTipo).build().perform();
	}
	
	public void btnSubMenuTshits() {
		btnSubMenuTshirts.click();
	}
	
	public void btnSubMenuBlouses() {
		btnSubMenuBlouses.click();
	}
	
	public void btnPrimerElemento() {
		withAction().moveToElement(btnPrimerElemento).build().perform();
	}
	
	public void btnAddToCart() {
		btnAddToCart.click();
	}
	
	public void validarPrecios(String precioBase,String precioIva,String precioTotal) {
		assertThat(precioBase, containsString(lblSubtotal.getText()));
		assertThat(precioIva, containsString(lblIva.getText()));
		assertThat(precioTotal, containsString(lblTotal.getText()));
	}
	
}
