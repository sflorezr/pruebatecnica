package co.com.sofka.pageobjets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
@DefaultUrl("http://automationpractice.com/index.php")
public class InicioSesionPage extends PageObject{

	@FindBy(className="login")
	WebElementFacade btnSignIn;
	
	@FindBy(id="email")
	WebElementFacade txtEmail;
	
	@FindBy(id="passwd")
	WebElementFacade txtPassword;		

	@FindBy(id="SubmitLogin")
	WebElementFacade SubmitLogin;	
	
	@FindBy(xpath="//*[@id='center_column']/div[1]/ol/li")
	WebElementFacade lblMensaje;
	
	public void clicSignIn() {
		btnSignIn.click();
	}
	
	public void digitarEmailyPassword(String email,String password) {
		txtEmail.type(email);
		txtPassword.type(password);
		SubmitLogin.click();
	}
	
	public void validaMensaje(String mensaje) {
		assertThat(mensaje, containsString(lblMensaje.getText()));
	}
}
