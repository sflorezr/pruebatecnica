package co.com.sofka.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features ="src/test/resources/features/pruebasInicioSesion.feature" , glue = "co.com.sofka.definitions")				
public class IniciarSesionRunner {

}
