package co.com.sofka.steps;

import co.com.sofka.pageobjets.ValidarPreciosPage;
import net.thucydides.core.annotations.Step;

public class ValidarPreciosStep {
	ValidarPreciosPage validarPreciosPage;
	@Step
	public void seleccionarClasificacion(String clasificacion){
		validarPreciosPage.btnTipo();
		switch (clasificacion) {
		case "T-Shirts":
			validarPreciosPage.btnSubMenuTshits();
			break;
		case "Blouses":
			validarPreciosPage.btnSubMenuBlouses();
			break;
		default:
			break;
		}
	}
	
	@Step
	public void aņadirAlCarro() {
		validarPreciosPage.btnPrimerElemento();
		validarPreciosPage.btnAddToCart();
	}
	
	@Step
	public void validarPrecios(String precioBase,String precioIva,String precioTotal) {
		validarPreciosPage.validarPrecios(precioBase, precioIva, precioTotal);
	}

}
