package co.com.sofka.steps;

import net.thucydides.core.annotations.Step;
import co.com.sofka.pageobjets.InicioSesionPage;

public class InicioSesionStep {
	InicioSesionPage inicioSesionPage;
	@Step
	public void abrirAplicativo() {
		inicioSesionPage.open();
	}
	
	@Step
	public void iraIniciarSesion() {
		inicioSesionPage.clicSignIn();
	}
	
	@Step
	public void digitarUsuarioyEmail(String email,String password) {
		inicioSesionPage.digitarEmailyPassword(email, password);
	}
	
	@Step
	public void validarMensaje(String mensaje) {
		inicioSesionPage.validaMensaje(mensaje);
	}
}
