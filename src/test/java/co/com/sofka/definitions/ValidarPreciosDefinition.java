package co.com.sofka.definitions;

import co.com.sofka.steps.ValidarPreciosStep;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ValidarPreciosDefinition {
	@Steps
	ValidarPreciosStep validarPreciosStep;
	
	@When("^Selecciono clasificacion \"([^\"]*)\"$")
	public void selecciono_clasificacion(String clasificacion) throws Exception {
		validarPreciosStep.seleccionarClasificacion(clasificacion);	    
	}
	
	@When("^Agregar al carro$")
	public void agregar_al_carro() throws Exception {
	    
		validarPreciosStep.aņadirAlCarro();
	}
	
	@Then("^Valido precios \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void valido_precios(String precioBase, String precioIva, String precioFinal) throws Exception {
	    
		validarPreciosStep.validarPrecios(precioBase, precioIva, precioFinal);
	}

}
