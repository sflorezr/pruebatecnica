package co.com.sofka.definitions;

import co.com.sofka.steps.InicioSesionStep;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class InicioSesionDefinition {
	
	@Steps
	InicioSesionStep inicioSesionStep;
	
	@Given("^Me encuentro en aplicativo$")
	public void me_encuentro_en_aplicativo() throws Exception { 
		inicioSesionStep.abrirAplicativo();
	}

	@Given("^Estoy en pagina de logueo$")
	public void estoy_en_pagina_de_logueo() throws Exception {	    
		inicioSesionStep.iraIniciarSesion();
	}

	@When("^Digito datos de usuario \"([^\"]*)\" y \"([^\"]*)\"$")
	public void digito_datos_de_usuario_y(String email, String password) throws Exception {	    
		inicioSesionStep.digitarUsuarioyEmail(email, password);
	}

	@Then("^Valido mensajes de error dependiendo del caso de prueba \"([^\"]*)\"$")
	public void valido_mensajes_de_error_dependiendo_del_caso_de_prueba(String mensaje) throws Exception {
		inicioSesionStep.validarMensaje(mensaje);
	    
	}
}
