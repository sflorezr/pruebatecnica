Feature: Pruebas inicio de sesion

  Scenario Outline: Casos de prueba inicio de sesion
    Given Me encuentro en aplicativo
    And Estoy en pagina de logueo
    When Digito datos de usuario "<email>" y "<password>"
    Then Valido mensajes de error dependiendo del caso de prueba "<mensaje>"

    Examples: 
      | email             | password | mensaje                    |
      |                   |          | An email address required. |
      |                   | prueba   | An email address required. |
      | correo@prueba.com |          | Password is required.      |
      | correoprueba.com  |          | Invalid email address.     |
      | correo@prueba.com | prueba   | Authentication failed.     |
