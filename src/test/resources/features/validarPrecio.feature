Feature: Validar Precios

  Scenario Outline: Validar Precios de articulos
    Given Me encuentro en aplicativo
    When Selecciono clasificacion "<clasificacion>"
    And Agregar al carro
    Then Valido precios "<precioSinIva>","<precioIva>","<precioFinal>"

    Examples: 
      | clasificacion | precioSinIva | precioIva | precioFinal |
      | T-Shirts      | $16.51       | $2.00     | $18.51      |
      | Blouses       | $27.00       | $2.00     | $29.00      |      